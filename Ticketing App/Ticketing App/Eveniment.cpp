#include "Eveniment.h"
#include <string>
#include <fstream>
#include <iomanip>
using namespace std;
int Eveniment::nrEvenimente = 0;

Eveniment::Eveniment() :id_eveniment(++nrEvenimente)
{
	this->nume = "To be annouced";
	this->pret = 0;
	this->data = "To be announced.";
	this->locatie = nullptr;
	this->nrBilete = 0;
	this->bilete = nullptr;
}

Eveniment::Eveniment(int id_eveniment, string data, Locatie* locatie) :id_eveniment(id_eveniment)
{
	this->nume = "To be annouced";
	this->pret = 0;
	this->data = data;
	this->locatie = new Locatie(*locatie);
	this->nrBilete = 0;
	this->bilete = nullptr;
}

Eveniment::Eveniment(int id_eveniment, string data, Locatie* locatie, int nrBilete, Bilet* bilete) :id_eveniment(id_eveniment)
{
	this->nume = "To be annouced";
	this->pret = 0;
	this->data = data;
	this->locatie = new Locatie(*locatie);
	this->nrBilete = nrBilete;
	if (nrBilete > 0 && bilete != nullptr)
	{
		Bilet::resetNrBilete();
		this->bilete = new Bilet[nrBilete];
		for (int i = 0; i < nrBilete; i++)
			this->bilete[i] = bilete[i];
	}
	else this->bilete = nullptr;
}

Eveniment::~Eveniment()
{
	if (this->locatie != nullptr) {
		delete locatie;
		this->locatie = nullptr;
	}
	if (this->bilete != nullptr)
	{
		delete[] this->bilete;
		this->bilete = nullptr;
	}
}

Eveniment::Eveniment(const Eveniment& e) :id_eveniment(e.id_eveniment)
{
	this->nume = e.nume;
	this->pret = e.pret;
	this->data = e.data;
	if (e.locatie != nullptr)
		this->locatie = new Locatie(*(e.locatie));
	else this->locatie = nullptr;
	this->nrBilete = e.nrBilete;
	if (e.nrBilete > 0 && e.bilete != nullptr)
	{
		this->bilete = new Bilet[e.nrBilete];
		for (int i = 0; i < e.nrBilete; i++)
			this->bilete[i] = e.bilete[i];
	}
	else this->bilete = nullptr;
}

class Eveniment& Eveniment::operator=(const Eveniment& e)
{
	if (this->locatie != nullptr)
	{
		delete this->locatie;
		this->locatie = nullptr;
	}
	if (this->bilete != nullptr)
	{
		delete[] bilete;
		this->bilete = nullptr;
	}
	this->data = e.data;
	this->nume = e.nume;
	this->pret = e.pret;
	if (e.locatie != nullptr)
		this->locatie = new Locatie(*(e.locatie));
	else this->locatie = nullptr;
	this->nrBilete = e.nrBilete;
	if (e.nrBilete > 0 && e.bilete != nullptr)
	{
		this->bilete = new Bilet[e.nrBilete];
		for (int i = 0; i < e.nrBilete; i++)
			this->bilete[i] = e.bilete[i];
	}
	else this->bilete = nullptr;
	return *this;
}

void Eveniment::prelucrare()
{
	if (this->bilete == nullptr)
	{
		this->nrBilete = Zona::total_locuri(this->locatie->getNrZone(), this->locatie->getShallowZone());
		Bilet::resetNrBilete();
		this->bilete = new Bilet[this->nrBilete];
	}
	int ZONA = 0;
	int contor_zona = 0;
	int rand = 1;
	int loc = 1;
	int loc_max = this->locatie->getShallowZone()[0].getLocuri();

	string initev;

	for (int i = 0; i < nume.length() && i < 9; ++i)
	{
		if (isalpha(nume[i]))
		{
			char c = char(nume[i]);
			int a = (int)c;
			a = i * a * i + i;
			a = a ^ 1;
			a = a % 25;
			c = (char)a;
			c = c + 'A';
			initev += c;
		}
	}

	for (int i = 0; i < this->nrBilete; i++, contor_zona++)
	{
		if (contor_zona == this->locatie->getShallowZone()[ZONA].locuri_zona())
		{
			contor_zona = 0;
			++ZONA;
			rand = 1;
			loc = 1;
			loc_max = this->locatie->getShallowZone()[ZONA].getLocuri();
		}
		bilete[i].setIdEveniment(this->id_eveniment);
		this->bilete[i].setZona(ZONA);
		if (loc > loc_max)
		{
			loc = 1;
			rand++;
		}
		this->bilete[i].setLoc(loc++);
		this->bilete[i].setRand(rand);
		this->bilete[i].appendId(initev);
	}
}

ostream& operator<<(ostream& out, Eveniment& e)
{
	out << "=========================== EVENIMENT ===========================\n";
	out << "\n\nNume eveniment: " << e.nume << "\n\n";
	out << "Data: " << e.data << "\n\n";
	out << "Pret Bilete -> Normal: " << e.pret << "   VIP: " << 3 * e.pret << "\n\n\n";
	out << *(e.locatie) << "\n";
	out << "Bilete puse spre vanzare: " << e.nrBilete << "\n\n";
	//for (int i = 0; i < e.nrBilete; i++)
		//out << e.bilete[i];
	out << "=================================================================\n\n\n";
	return out;
}

bool Eveniment::operator>(const Eveniment& e)
{
	return this->nrBilete > e.nrBilete;
}

Bilet& Eveniment::operator[](int i)
{
	if (i >= 0 && i < this->nrBilete)
		return this->bilete[i];
	else
		throw "Index invalid";
}

void Eveniment::setData(string data)
{
	this->data = data;
}

void Eveniment::setLocatie(Locatie* l)
{
	delete this->locatie;
	this->locatie = new Locatie(*l);
}

void Eveniment::setBilete(int nrBilete, Bilet* bilete)
{
	if (nrBilete > 0 && bilete != nullptr) {
		delete[] this->bilete;
		this->nrBilete = nrBilete;
		this->bilete = new Bilet[nrBilete];
		for (int i = 0; i < nrBilete; i++)
			this->bilete[i] = bilete[i];
	}
}

int Eveniment::getIdEveniment() { return this->id_eveniment; }
string Eveniment::getData() { return this->data; }
string Eveniment::getNume() { return this->nume; }
int Eveniment::getPret() { return this->pret; }
Locatie* Eveniment::getLocatie()
{
	if (this->locatie != nullptr)
	{
		Locatie* p = new Locatie(*(this->locatie));
		return p;
	}
	return nullptr;
}

int Eveniment::getNrBilete() { return this->nrBilete; }

Bilet* Eveniment::getBilete() {
	if (this->nrBilete > 0 && bilete != nullptr)
	{
		Bilet* b = new Bilet[this->nrBilete];
		for (int i = 0; i < this->nrBilete; i++)
			b[i] = this->bilete[i];
		return b;
	}
	return nullptr;
}

istream& operator>>(istream& in, Eveniment& e)
{
	cout << "===Citirea evenimentului:===\n";
	string buffer;
	while (true)
	{
		cout << "Introduceti numele evenimentului: ";
		getline(in, buffer);
		if (buffer.length() > 3)
			break;
	}
	e.nume = buffer;
	while (true)
	{
		cout << "Introduceti pretul unui bilet: ";
		getline(in, buffer);
		if (buffer.length() > 0 && buffer.length() < 8)
			break;
	}
	e.pret = stoi(buffer);
	while (true)
	{
		cout << "Introduceti data in formatul DD-MM-YYYY: ";
		getline(in, buffer);
		if (buffer.length() > 9)
			break;
	}
	e.data = buffer;
	if (e.locatie != nullptr)
		delete e.locatie;
	e.locatie = new Locatie();
	cin >> (*e.locatie);
	cout << "Biletele vor fi generate automat :)\n";
	e.prelucrare();
	return in;
}

ifstream& operator>>(ifstream& in, Eveniment& e)
{
	string buffer;
	getline(in, buffer);
	e.nume = buffer;
	getline(in, buffer);
	e.data = buffer;
	getline(in, buffer);
	e.pret = stoi(buffer);
	if (e.locatie != nullptr)
		delete e.locatie;
	e.locatie = new Locatie();
	in >> *(e.locatie);
	return in;
}

ofstream& operator<<(ofstream& out, Eveniment& e)
{
	out << e.nume << endl;
	out << e.data << endl;
	out << e.pret << endl;
	out << (*e.locatie);
	return out;
}

int Eveniment::getBileteRamase()
{
	int ocupate = 0;
	for (int i = 0; i < nrBilete; i++)
		if (bilete[i].isOcupat())
			ocupate++;
	return nrBilete - ocupate;
}

int Eveniment::getBileteRamaseZona(int nrZona)
{
	int nrBileteZonaOcupate = 0;
	if (nrZona < locatie->getNrZone() && nrZona >= 0)
	{
		for (int i = 0; i < nrBilete; i++)
			if (bilete[i].getZona() == nrZona)
				if (bilete[i].isOcupat())
					++nrBileteZonaOcupate;
		return locatie->getShallowZone()[nrZona].getLocuriRamase(nrBileteZonaOcupate);
	}
	return 0;
}

Bilet* Eveniment::getShallowBilete()
{
	return this->bilete;
}

Locatie* Eveniment::getShallowLocatie()
{
	return this->locatie;
}


void Eveniment::afisareLocuri(int nrZona)
{
	int k = 0, loc = 0, rand = 0;
	string buffer;
	string randFormat = "Randul ";
	for (int i = 0; i < locatie->getShallowZone()[nrZona].getRanduri(); ++i)
	{
		buffer = randFormat + to_string(++rand);
		cout << setw(9) << setfill(' ') << buffer;
		cout << ": ";
		for (int j = 0; j < locatie->getShallowZone()[nrZona].getLocuri();)
		{
			if (bilete[k].getZona() == nrZona) {
				++j;
				if (bilete[k].isOcupat())
					cout << "XX ";
				else cout << setw(2) << setfill('0') << j << " ";
			}
			++k;
		}
		cout << "\n";
	}
	cout << "\n\n";
}

Bilet Eveniment::cumparaBilet(int nrZona, int nrRand, int nrLoc)
{
	int bileteInainte = 0;
	for (int i = 0; i < nrZona; i++)
		bileteInainte += locatie->getShallowZone()[i].locuri_zona();
	bilete[bileteInainte + nrRand * locatie->getShallowZone()[nrZona].getLocuri() + nrLoc].cumparaBilet();
	return bilete[bileteInainte + nrRand * locatie->getShallowZone()[nrZona].getLocuri() + nrLoc];
}

int Eveniment::getNrEvenimente()
{
	return Eveniment::nrEvenimente;
}

void Eveniment::decreaseNrEvenimente(int i)
{
	Eveniment::nrEvenimente -= i;
}