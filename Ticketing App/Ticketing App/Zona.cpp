#include "Zona.h"
#include <string>
#include <cctype>
#include <fstream>
Zona::Zona()
{
	nume = "default";
	vip = false;
	randuri = 0;
	locuri = 0;
}

Zona::Zona(string nume, bool vip, int randuri, int locuri)
{
	this->nume = nume;
	this->vip = vip;
	this->randuri = randuri;
	this->locuri = locuri;
}

void Zona::setNume(string nume) { this->nume = nume; }
void Zona::setVip(bool vip) { this->vip = vip; }
void Zona::setRanduri(int randuri) { this->randuri = randuri; }
void Zona::setLocuri(int locuri) { this->locuri = locuri; }

// Getters
string Zona::getNume() { return nume; }
bool Zona::getVip() { return vip; }
int Zona::getRanduri() { return randuri; }
int Zona::getLocuri() { return locuri; }

int Zona::total_locuri(int nrZone, Zona* zone)
{
	int total = 0;
	for (int i = 0; i < nrZone; i++)
	{
		total += zone[i].randuri * zone[i].locuri;
	}
	return total;
}

int Zona::locuri_zona()
{
	return this->randuri * this->locuri;
}

ostream& operator<<(ostream& out, Zona& z)
{
	//out << "====== Zona ======\n";
	out << "Nume: " << z.nume << "\n";
	out << "Vip: " << (z.vip ? "Da" : "Nu") << "\n";
	//out << "Randuri: " << z.randuri << "\n";
	//out << "Scaune/Rand: " << z.locuri << "\n";
	//out << "==================\n";
	return out;
}

istream& operator>>(istream& in, Zona& z)
{
	cout << "=Citirea zonei=\n";
	string buffer;
	while (true) {
		cout << "Introduceti numele zonei: ";
		getline(in, buffer);
		if (buffer.length() > 0)
			break;
	}
	z.nume = buffer;
	while (true) {
		cout << "VIP (Da sau Nu): ";
		getline(in, buffer);
		while (buffer.length() == 0)
		{
			cout << "VIP (Da sau Nu): ";
			getline(in, buffer);
		}
		for (int i = 0; i < buffer.length(); i++)
			buffer[i] = tolower(buffer[i]);
		if (buffer.length() == 2 && (buffer == "da" || buffer == "nu"))
			break;
	}
	buffer == "da" ? z.vip = true : z.vip = false;
	while (true) {
		int Digit = 1;
		cout << "Numarul de randuri: ";
		getline(in, buffer);
		while (buffer.length() == 0)
		{
			cout << "Numarul de randuri: ";
			getline(in, buffer);
		}
		for (int i = 0; i < buffer.length(); i++)
			if (!isdigit(buffer[i]))
			{
				Digit = 0;
				break;
			}
		if (Digit == 1 && buffer.length() < 8 && stoi(buffer) > 0)
			break;
	}
	z.randuri = stoi(buffer);
	while (true) {
		int Digit = 1;
		cout << "Numarul de locuri/rand: ";
		getline(in, buffer);
		while (buffer.length() == 0)
		{
			cout << "Numarul de locuri/rand: ";
			getline(in, buffer);
		}
		for (int i = 0; i < buffer.length(); i++)
			if (!isdigit(buffer[i]))
			{
				Digit = 0;
				break;
			}
		if (Digit == 1 && buffer.length() < 8 && stoi(buffer) > 0)
			break;
	}
	z.locuri = stoi(buffer);
	return in;
}

ifstream& operator>>(ifstream& in, Zona& z)
{
	string buffer;
	getline(in, buffer);
	z.nume = buffer;
	getline(in, buffer);
	for (int i = 0; i < buffer.length(); i++)
		buffer[i] = tolower(buffer[i]);
	buffer == "da" ? z.vip = true : z.vip = false;
	getline(in, buffer);
	z.randuri = stoi(buffer);
	getline(in, buffer);
	z.locuri = stoi(buffer);
	return in;
}

ofstream& operator<<(ofstream& out, Zona& z)
{
	out << z.nume << endl;
	if (z.vip == true)
		out << "da";
	else out << "nu";
	out << endl;
	out << z.randuri << endl;
	out << z.locuri << endl;
	return out;
}

int Zona::getLocuriRamase(int ocupate)
{
	return locuri * randuri - ocupate;
}

Zona::operator bool()
{
	return this->vip;
}

const Zona Zona::operator++()
{
	this->vip = true;
	return *this;
}

const Zona Zona::operator++(int)
{
	Zona copie = *this;
	this->vip = true;
	return copie;
}