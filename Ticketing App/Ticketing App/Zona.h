#pragma once
#include <iostream>
using namespace std;

class Zona
{
private:
	string nume;
	bool vip;
	int randuri;
	int locuri;
public:
	Zona();
	Zona(string nume, bool vip, int randuri, int locuri);
	//setters
	void setNume(string nume);
	void setVip(bool vip);
	void setRanduri(int randuri);
	void setLocuri(int locuri);
	//getters
	string getNume();
	bool getVip();
	int getRanduri();
	int getLocuri();

	int getLocuriRamase(int ocupate);

	int locuri_zona();
	static int total_locuri(int nrZone, Zona* zone);
	friend ostream& operator<<(ostream& out, Zona& z);
	friend istream& operator>>(istream& in, Zona& z);
	friend ifstream& operator>>(ifstream& in, Zona& z);
	friend ofstream& operator<<(ofstream& out, Zona& z);
	explicit operator bool();
	const Zona operator++();
	const Zona operator++(int);
};

