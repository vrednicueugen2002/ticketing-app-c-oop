#include "Zona.h"
#include "Locatie.h"
#include "Bilet.h"
#include "Eveniment.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>


int main(int argc, char* argv[])
{
	Eveniment* e = nullptr;
	int nrEv = 0;
	vector<Bilet> bileteCump;

	if (argc > 1)
	{
		string buffer;
		ifstream f;
		f.open(argv[1], ios::in);
		getline(f, buffer);
		nrEv = stoi(buffer);
		e = new Eveniment[nrEv];
		for (int i = 0; i < nrEv; i++) {
			f >> e[i];
			e[i].prelucrare();
		}

		int nrBileteCump;
		Bilet bufferB;
		ifstream g("bilete.bin", ios::in | ios::binary);
		g.read((char*)&nrBileteCump, sizeof(nrBileteCump));
		g.close();
		int poz = sizeof(nrBileteCump);
		for (int i = 0; i < nrBileteCump; i++)
		{
			poz += bufferB.deSerializare(poz);
			bileteCump.push_back(bufferB);
		}


		for (auto i = bileteCump.begin(); i != bileteCump.end(); ++i)
			for (int j = 0; j < nrEv; j++)
			{
				if ((*i).getIdEveniment() == e[j].getIdEveniment())
				{
					int size = 0;
					string id = (*i).getId();
					for (int k = 0; k < id.length(); k++)
					{
						if (isdigit(id[k]))
							++size;
						else break;
					}
					int nrBilet = stoi(id.substr(0, size));
					if (e[j].getShallowBilete()[nrBilet] == (*i))
						e[j].getShallowBilete()[nrBilet].cumparaBilet();
					else std::cout << "Bilet invalid";
					break;
				}
			}
	}

	int input = -1;
	int inputEv = -1;
	int inputZona = -1;
	int inputRand = -1;
	int inputLoc = -1;
	//int bOk = 0;
	string finalTranz;
	string confirmare = "DA";
	Eveniment a;
	Eveniment::decreaseNrEvenimente(1);

	while (input)
	{
		std::cout << "\n\n1) Evenimente disponibile\n";
		std::cout << "2) Cumpara un bilet\n";
		std::cout << "3) Verifica un bilet\n";
		std::cout << "99) Adauga un eveniment\n";
		std::cout << "0) Inchide meniul\n";
		std::cout << "\nAlege o optiune dintre cele de mai sus: ";
		std::cin >> input;
		switch (input)
		{
		case 1:
			std::cout << "\n\n";
			for (int i = 0; i < nrEv; i++) {
				std::cout << "\tEveniment (" << i + 1 << ")\n";
				std::cout << "Nume: " << e[i].getNume() << "\n" << "Data: " << e[i].getData() <<
					"\n" << "Preturi: Normal: " << e[i].getPret() << "   VIP: " << 3 * e[i].getPret()
					<< "\nBilete ramase: " << e[i].getBileteRamase() << "\n\n";
			}
			std::cout << "\n";
			break;
		case 2:
			if (nrEv == 0) {
				std::cout << "\n\nNe pare rau, momentan nu exista evenimente disponibile.\n";
				break;
			}
			std::cout << "\n\nAlege un eveniment:\n";
			for (int i = 0; i < nrEv; i++)
				std::cout << i << ") " << e[i].getNume() << "\n";
			std::cin >> inputEv;
			if (inputEv >= 0 && inputEv <= nrEv - 1)
			{
				std::cout << "Ati ales evenimentul :" << e[inputEv].getNume() << "\n\n";
				std::cout << "Exista " << e[inputEv].getShallowLocatie()->getNrZone() << " zone:\n\n";
				for (int i = 0; i < e[inputEv].getShallowLocatie()->getNrZone(); ++i) {
					std::cout << "=== Zona: " << i + 1 << " ===\n";
					std::cout << e[inputEv].getShallowLocatie()->getShallowZone()[i];
					std::cout << "Bilete ramase: " << e[inputEv].getBileteRamaseZona(i) << "\n\n";
				}
				std::cout << "Alegeti o zona: ";
				std::cin >> inputZona;
				std::cout << "\n";
				--inputZona;
				if (inputZona >= 0 && inputZona <= e[inputEv].getShallowLocatie()->getNrZone() - 1)
				{
					if (e[inputEv].getBileteRamaseZona(inputZona) > 0)
					{
						e[inputEv].afisareLocuri(inputZona);
						std::cout << "Alegeti un rand: ";
						std::cin >> inputRand;
						--inputRand;
						std::cout << "Alegeti un loc: ";
						std::cin >> inputLoc;
						--inputLoc;
						if (inputRand > e[inputEv].getShallowLocatie()->getShallowZone()[inputZona].getRanduri() - 1 ||
							inputLoc > e[inputEv].getShallowLocatie()->getShallowZone()[inputZona].getLocuri() - 1
							|| inputRand < 0 || inputLoc < 0) {
							std::cout << "Rand sau loc incorect\n";
							std::cout << "Randuri :" << e[inputEv].getShallowLocatie()->getShallowZone()[inputZona].getRanduri() << endl;
							std::cout << "Locuri: " << e[inputEv].getShallowLocatie()->getShallowZone()[inputZona].getLocuri();
						}
						else
						{
							cout << "Pretul unui bilet la aceasta zona este de: " << 2 * e[inputEv].getPret() *
								e[inputEv].getShallowLocatie()->getShallowZone()[inputZona].getVip() + e[inputEv].getPret() << " lei.";
							cout << "\nDoresti sa finalizezi achizitia? (DA / NU): ";
							cin >> finalTranz;
							if (finalTranz.length() < 3 && finalTranz == confirmare) {
								int bOk = 0;
								Bilet bCheck = e[inputEv].cumparaBilet(inputZona, inputRand, inputLoc);
								//bileteCump.push_back(e[inputEv].cumparaBilet(inputZona, inputRand, inputLoc));
								for(auto i = bileteCump.begin(); i != bileteCump.end(); ++i)
									if (*i == bCheck)
									{
										bOk = 1;
										break;
									}
								if (!bOk) {
									std::cout << "\nBilet cumparat cu succes: ";
									cout << bCheck;
									bileteCump.push_back(bCheck);
									//std::cout << (*(bileteCump.end() - 1)).getId();
									std::cout << "\n";
								}
							}
							else cout << "Tranzactie anulata!";
						}
					}
				}

			}
			break;
		case 99:
		{
			int cod = 123, buffer;
			std::cout << "Introdu codul de admin: ";
			std::cin >> buffer;
			if (buffer != cod)
			{
				std::cout << "Cod Invalid.";
				break;
			}
			getchar();
			Eveniment a;
			std::cin >> a;
			nrEv++;
			Eveniment* b = new Eveniment[nrEv];
			Eveniment::decreaseNrEvenimente(nrEv);
			for (int i = 0; i < nrEv - 1; i++)
				b[i] = e[i];
			b[nrEv - 1] = a;
			delete[] e;
			e = b;
			b = nullptr;
			std::cout << "Eveniment adaugat cu succes!";
		}
		break;
		case 3:
		{
			int ok = 0;
			string buffer;
			std::cout << "Introdu id-ul biletului de verificat: ";
			std::cin >> buffer;
			for (auto i = bileteCump.begin(); i != bileteCump.end(); ++i)
				if (buffer == (*i).getId())
				{
					std::cout << "Biletul este valid pentru evenimentul ";
					std::cout << (*i).getIdEveniment();
					std::cout << "\n";
					ok = 1;
					break;
				}
			if (!ok) std::cout << "Id-ul introdus nu se regaseste in lista de bilete.\n";
		}
		break;
		default:
			break;
		}
	}
	ofstream g("bilete.bin", ios::out | ios::trunc | ios::binary);
	int nrBileteCumparate = bileteCump.size();
	g.write((char*)&nrBileteCumparate, sizeof(nrBileteCumparate));
	g.close();
	if (nrBileteCumparate > 0)
		for (int i = 0; i < nrBileteCumparate; i++)
			bileteCump[i].serializare();
	if (nrEv > 0) {
		ofstream h("date.txt", ios::out | ios::trunc);
		h << nrEv << endl;
		for (int i = 0; i < nrEv; i++)
			h << e[i];
		h.close();
	}
	return 0;
}