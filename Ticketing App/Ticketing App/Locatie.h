#pragma once
#include "Zona.h"

class Locatie
{
private:
	string adresa;
	char* denumire;
	int nrZone;
	Zona* zone;
public:
	Locatie();
	Locatie(string adresa, const char* denumire, int nrZone, Zona* zone);
	~Locatie();
	Locatie(const Locatie& l);
	void setAdresa(string adresa);
	void setDenumire(const char* denumire);
	void setNrZone(int nrZone);
	void setZone(int nrZone, Zona* zone);
	string getAdresa();
	char* getDenumire();
	int getNrZone();
	Zona* getZone();
	Zona* getShallowZone();
	Locatie& operator=(const Locatie& l);
	friend ostream& operator<<(ostream& out, Locatie& l);
	friend istream& operator>>(istream& in, Locatie& l);
	friend ifstream& operator>>(ifstream& in, Locatie& l);
	friend ofstream& operator<<(ofstream& out, Locatie& l);
	//void serializare();
};

