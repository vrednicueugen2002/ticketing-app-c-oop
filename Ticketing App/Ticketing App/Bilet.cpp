#include "Bilet.h"
int Bilet::nrBilete = 0;

Bilet::Bilet()
{
	this->id_eveniment = -1;
	this->zona = -1;
	this->rand = -1;
	this->loc = -1;
	this->id = to_string(++Bilet::nrBilete);
	this->ocupat = false;
}

Bilet::Bilet(int id_eveniment, int zona, int rand, int loc, bool ocupat)
{
	this->id_eveniment = id_eveniment;
	this->zona = zona;
	this->rand = rand;
	this->loc = loc;
	this->id = to_string(++Bilet::nrBilete);
	this->ocupat = ocupat;
}

ostream& operator<<(ostream& out, Bilet& b)
{
	out << "ID_EV: " << b.id_eveniment;
	out << " Zona: " << b.zona + 1;
	out << " Rand: " << b.rand;
	out << " Loc: " << b.loc;
	out << " ID-" << b.id;
	out << "\n";
	return out;
}

istream& operator>>(istream& in, Bilet& b)
{
	string buffer;
	while (true) {
		int Digit = 1;
		cout << "Introduceti ID-ul evenimentului: ";
		getline(in, buffer);
		while (buffer.length() == 0)
		{
			cout << "Introduceti ID-ul evenimentului: ";
			getline(in, buffer);
		}
		for (int i = 0; i < buffer.length(); i++)
			if (!isdigit(buffer[i]))
			{
				Digit = 0;
				break;
			}
		if (Digit == 1 && buffer.length() < 8)
			break;
	}
	b.id_eveniment = stoi(buffer);
	while (true) {
		int Digit = 1;
		cout << "Introduceti zona: ";
		getline(in, buffer);
		while (buffer.length() == 0)
		{
			cout << "Introduceti zona: ";
			getline(in, buffer);
		}
		for (int i = 0; i < buffer.length(); i++)
			if (!isdigit(buffer[i]))
			{
				Digit = 0;
				break;
			}
		if (Digit == 1 && buffer.length() < 8 && stoi(buffer) > 0)
			break;
	}
	b.zona = stoi(buffer) - 1;
	while (true) {
		int Digit = 1;
		cout << "Introduceti randul: ";
		getline(in, buffer);
		while (buffer.length() == 0)
		{
			cout << "Introduceti randul: ";
			getline(in, buffer);
		}
		for (int i = 0; i < buffer.length(); i++)
			if (!isdigit(buffer[i]))
			{
				Digit = 0;
				break;
			}
		if (Digit == 1 && buffer.length() < 8 && stoi(buffer) > 0)
			break;
	}
	b.rand = stoi(buffer);
	while (true) {
		int Digit = 1;
		cout << "Introduceti locul: ";
		getline(in, buffer);
		while (buffer.length() == 0)
		{
			cout << "Introduceti locul: ";
			getline(in, buffer);
		}
		for (int i = 0; i < buffer.length(); i++)
			if (!isdigit(buffer[i]))
			{
				Digit = 0;
				break;
			}
		if (Digit == 1 && buffer.length() < 8 && stoi(buffer) > 0)
			break;
	}
	b.loc = stoi(buffer);
	while (true) {
		cout << "Introduceti id-ul biletului: ";
		getline(in, buffer);
		if (buffer.length() > 0)
			break;
	}
	b.id = buffer;
	b.ocupat = 1;
	return in;
}

bool Bilet::operator==(const Bilet& b)
{
	if (this->id_eveniment != b.id_eveniment || this->zona != b.zona ||
		this->loc != b.loc || this->id != b.id)
		return false;
	return true;
}

Bilet::operator string()
{
	return this->id;
}

//setters

void Bilet::setIdEveniment(int idEveniment) { id_eveniment = idEveniment; }
void Bilet::setZona(int zona) { this->zona = zona; }
void Bilet::setRand(int rand) { this->rand = rand; }
void Bilet::setLoc(int loc) { this->loc = loc; }
void Bilet::setId(string id) { this->id = id; }
void Bilet::setOcupat(bool ocupat) { this->ocupat = ocupat; }
void Bilet::resetNrBilete() { Bilet::nrBilete = -1; }

// Getters
int Bilet::getIdEveniment() { return id_eveniment; }
int Bilet::getZona() { return zona; }
int Bilet::getRand() { return rand; }
int Bilet::getLoc() { return loc; }
string Bilet::getId() { return id; }
bool Bilet::isOcupat() { return ocupat; }

void Bilet::cumparaBilet()
{
	if (ocupat)
		cout << "Acest loc este deja ocupat!";
	else
	{
		ocupat = true;
	}
}

void Bilet::serializare()
{
	ofstream f("bilete.bin", ios::app | ios::binary);
	f.write((char*)&id_eveniment, sizeof(id_eveniment));
	f.write((char*)&zona, sizeof(zona));
	f.write((char*)&rand, sizeof(rand));
	f.write((char*)&loc, sizeof(loc));
	int length = id.length();
	f.write((char*)&length, sizeof(length));
	f.write(id.c_str(), length + 1);
	f.write((char*)&ocupat, sizeof(ocupat));
	f.close();
}


int Bilet::deSerializare(int i)
{
	ifstream f("bilete.bin", ios::in | ios::binary);
	f.seekg(i);
	f.read((char*)&id_eveniment, sizeof(id_eveniment));
	f.read((char*)&zona, sizeof(zona));
	f.read((char*)&rand, sizeof(rand));
	f.read((char*)&loc, sizeof(loc));
	int length;
	f.read((char*)&length, sizeof(length));
	char* sir = new char[length + 1];
	f.read(sir, length + 1);
	id = sir;
	delete[] sir;
	f.read((char*)&ocupat, sizeof(ocupat));
	f.close();
	return sizeof(id_eveniment) + sizeof(zona) + sizeof(rand) + sizeof(loc) +
		sizeof(length) + length + 1 + sizeof(ocupat);
}

int Bilet::getNumarId()
{
	string buffer;
	for (int i = 0; i < id.length(); i++)
	{
		if (isdigit(id[i]))
			buffer += to_string(id[i]);
		else break;
	}
	return stoi(buffer);
}

void Bilet::appendId(string append)
{
	if (append.length() > 0)
		this->id += append;
}