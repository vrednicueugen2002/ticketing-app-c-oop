#pragma once
#pragma warning(disable:6031)
#pragma warning(disable:4267)
#include <iostream>
#include <string>
#include <fstream>
#include "Serializare.h"
using namespace std;

class Bilet : public Serializare
{
private:
	int id_eveniment;
	int zona;
	int rand;
	int loc;
	string id;
	bool ocupat;
	static int nrBilete;
public:
	Bilet();
	Bilet(int id_eveniment, int zona, int rand, int loc, bool ocupat);
	friend ostream& operator<<(ostream& out, Bilet& b);
	friend istream& operator>>(istream& in, Bilet& b);
	//operators
	explicit operator string();
	bool operator==(const Bilet& b);
	//setters
	void setIdEveniment(int id_eveniment);
	void setZona(int zona);
	void setRand(int rand);
	void setLoc(int loc);
	void setId(string id);
	void setOcupat(bool ocupat);
	static void resetNrBilete();
	//getters
	int getIdEveniment();
	int getZona();
	int getRand();
	int getLoc();
	string getId();
	int getNumarId();
	void appendId(string append);

	bool isOcupat();

	void cumparaBilet();

	void serializare();
	int deSerializare(int i);
};

