#include "Locatie.h"
#include <string>
#include <fstream>
Locatie::Locatie()
{
	adresa = "undefined";
	denumire = new char[strlen("undefined") + 1];
	strcpy_s(denumire, strlen("undefined") + 1, "undefined");
	nrZone = 0;
	zone = nullptr;
}

Locatie::Locatie(string adresa, const char* denumire, int nrZone, Zona* zone)
{
	this->adresa = adresa;
	if (strlen(denumire) > 0) {
		this->denumire = new char[strlen(denumire) + 1];
		strcpy_s(this->denumire, strlen(denumire) + 1, denumire);
	}
	else this->denumire = nullptr;
	this->nrZone = nrZone;
	if (nrZone > 0 && zone != nullptr)
	{
		this->zone = new Zona[nrZone];
		for (int i = 0; i < nrZone; i++)
			this->zone[i] = zone[i];
	}
	else this->zone = nullptr;
}

Locatie::~Locatie()
{
	if (this->denumire != nullptr)
	{
		delete this->denumire;
		this->denumire = nullptr;
	}
	if (this->zone != nullptr) {
		delete[] this->zone;
		this->zone = nullptr;
	}
}

Locatie::Locatie(const Locatie& l)
{
	this->adresa = l.adresa;
	this->denumire = new char[strlen(l.denumire) + 1];
	strcpy_s(this->denumire, strlen(l.denumire) + 1, l.denumire);
	this->nrZone = l.nrZone;
	if (l.nrZone > 0 && l.zone != nullptr)
	{
		this->zone = new Zona[l.nrZone];
		for (int i = 0; i < l.nrZone; i++)
			this->zone[i] = l.zone[i];
	}
	else this->zone = nullptr;
}

class::Locatie& Locatie::operator=(const Locatie& l)
{
	if (this->denumire != nullptr)
		delete[] this->denumire;
	if (this->zone != nullptr)
		delete[] this->zone;
	this->adresa = l.adresa;
	this->denumire = new char[strlen(l.denumire) + 1];
	strcpy_s(this->denumire, strlen(l.denumire) + 1, l.denumire);
	this->nrZone = l.nrZone;
	if (l.nrZone > 0 && l.zone != nullptr)
	{
		this->zone = new Zona[l.nrZone];
		for (int i = 0; i < l.nrZone; i++)
			this->zone[i] = l.zone[i];
	}
	else this->zone = nullptr;
	return *this;
}

void Locatie::setAdresa(string adresa) { this->adresa = adresa; }

void Locatie::setDenumire(const char* denumire)
{
	if (this->denumire != nullptr)
		delete[] this->denumire;
	this->denumire = new char[strlen(denumire) + 1];
	strcpy_s(this->denumire, strlen(denumire) + 1, denumire);
}

void Locatie::setNrZone(int nrZone) { this->nrZone = nrZone; }
void Locatie::setZone(int nrZone, Zona* zone)
{
	if (nrZone > 0 && zone != nullptr) {
		if (this->zone != nullptr)
			delete[] this->zone;
		this->nrZone = nrZone;
		this->zone = new Zona[nrZone];
		for (int i = 0; i < nrZone; i++)
			this->zone[i] = zone[i];
	}
}

string Locatie::getAdresa() { return this->adresa; }
char* Locatie::getDenumire()
{
	char* d = new char[strlen(this->denumire) + 1];
	strcpy_s(d, strlen(this->denumire) + 1, this->denumire);
	return d;
}
int Locatie::getNrZone() { return this->nrZone; }
Zona* Locatie::getZone()
{
	Zona* z = nullptr;
	if (this->nrZone > 0 && this->zone != nullptr) {
		z = new Zona[this->nrZone];
		for (int i = 0; i < this->nrZone; i++)
			z[i] = this->zone[i];
	}
	return z;
}



ostream& operator<<(ostream& out, Locatie& l)
{
	out << "=========== Locatie ===========\n";
	out << "Denumire: " << l.denumire << "\n";
	out << "Adresa: " << l.adresa << "\n";
	out << "Numar zone: " << l.nrZone << "\n";
	for (int i = 0; i < l.nrZone; i++)
		out << l.zone[i];
	out << "===============================\n";
	return out;
}

istream& operator>>(istream& in, Locatie& l)
{
	cout << "==Citirea locatiei==\n";
	string buffer;
	while (true)
	{
		cout << "Introduceti adresa locatiei: ";
		getline(in, buffer);
		if (buffer.length() > 0)
			break;
	}
	l.adresa = buffer;
	if (l.denumire != nullptr)
		delete[] l.denumire;
	while (true)
	{
		cout << "Introduceti numele locatiei: ";
		getline(in, buffer);
		if (buffer.length() > 0)
			break;
	}
	l.denumire = new char[buffer.length() + 1];
	strcpy_s(l.denumire, buffer.length() + 1, buffer.c_str());
	while (true) {
		int Digit = 1;
		cout << "Introduceti numarul de zone: ";
		getline(in, buffer);
		while (buffer.length() == 0)
		{
			cout << "Introduceti numarul de zone: ";
			getline(in, buffer);
		}
		for (int i = 0; i < buffer.length(); i++)
			if (!isdigit(buffer[i]))
			{
				Digit = 0;
				break;
			}
		if (Digit == 1 && buffer.length() < 8 && stoi(buffer) > 0)
			break;
	}
	l.nrZone = stoi(buffer);
	if (l.zone != nullptr)
		delete[] l.zone;
	l.zone = new Zona[l.nrZone];
	for (int i = 0; i < l.nrZone; i++)
		in >> l.zone[i];
	return in;
}

ifstream& operator>>(ifstream& in, Locatie& l)
{
	string buffer;
	getline(in, buffer);
	l.adresa = buffer;
	getline(in, buffer);
	if (l.denumire != nullptr)
		delete[] l.denumire;
	l.denumire = new char[buffer.length() + 1];
	strcpy_s(l.denumire, buffer.length() + 1, buffer.c_str());
	getline(in, buffer);
	l.nrZone = stoi(buffer);
	if (l.zone != nullptr)
		delete[] l.zone;
	l.zone = new Zona[l.nrZone];
	for (int i = 0; i < l.nrZone; i++)
		in >> l.zone[i];
	return in;
}

ofstream& operator<<(ofstream& out, Locatie& l)
{
	out << l.adresa << endl;
	out << l.denumire << endl;
	out << l.nrZone << endl;
	for (int i = 0; i < l.nrZone; i++)
		out << l.zone[i];
	return out;
}

Zona* Locatie::getShallowZone()
{
	return this->zone;
}