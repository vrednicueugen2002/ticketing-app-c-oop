#pragma once
#include <iostream>

class Serializare {
	virtual void serializare() = 0;
	virtual int deSerializare(int i) = 0;
};
