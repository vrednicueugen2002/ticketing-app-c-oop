#pragma once
#include "Locatie.h"
#include "Bilet.h"
class Eveniment
{
private:
	int id_eveniment;
	string nume;
	int pret;
	string data;
	Locatie* locatie;
	int nrBilete;
	Bilet* bilete;
	static int nrEvenimente;
public:
	Eveniment();
	Eveniment(int id_eveniment, string data, Locatie* locatie);
	Eveniment(int id_eveniment, string data, Locatie* locatie, int nrBilete, Bilet* bilete);
	~Eveniment();
	Eveniment(const Eveniment& e);
	Eveniment& operator=(const Eveniment& e);
	friend ostream& operator<<(ostream& out, Eveniment& e);
	friend istream& operator>>(istream& in, Eveniment& e);
	friend ifstream& operator>>(ifstream& in, Eveniment& e);
	friend ofstream& operator<<(ofstream& out, Eveniment& e);
	void prelucrare();
	bool operator>(const Eveniment& e);
	Bilet& operator[](int i);

	//setters
	void setData(string data);
	void setLocatie(Locatie* l);
	void setBilete(int nrBilete, Bilet* bilete);

	//getters
	int getIdEveniment();
	string getNume();
	string getData();
	int getPret();
	Locatie* getLocatie();
	int getNrBilete();
	static int getNrEvenimente();
	static void decreaseNrEvenimente(int i);


	Bilet* getBilete();
	Bilet* getShallowBilete();
	Locatie* getShallowLocatie();

	int getBileteRamase();
	int getBileteRamaseZona(int nrZona);

	void afisareLocuri(int nrZona);
	Bilet cumparaBilet(int nrZona, int nrRand, int nrLoc);
};

